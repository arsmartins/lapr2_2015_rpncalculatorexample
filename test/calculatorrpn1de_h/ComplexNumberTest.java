/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatorrpn1de_h;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Scanner;

/**
 *
 * @author amartins
 */
public class ComplexNumberTest {
    
    public ComplexNumberTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class ComplexNumber.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        ComplexNumber z = new ComplexNumber( -1.0, 2);
        ComplexNumber instance = new ComplexNumber( 1.2, -3);
        ComplexNumber expResult = new ComplexNumber( 0.2, -1);
        ComplexNumber result = instance.add(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of subtract method, of class ComplexNumber.
     */
    @Test
    public void testSubtract() {
        System.out.println("subtract");
        ComplexNumber z = new ComplexNumber( -1, 2);
        ComplexNumber instance = new ComplexNumber( 1.2, -3);
        ComplexNumber expResult = new ComplexNumber( 2.2, -5);
        ComplexNumber result = instance.subtract(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of multiply method, of class ComplexNumber.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        ComplexNumber z = new ComplexNumber( 1, 2);
        ComplexNumber instance = new ComplexNumber( 3, -1);
        ComplexNumber expResult = new ComplexNumber( 5, 5);
        ComplexNumber result = instance.multiply(z);
        assertEquals(expResult, result);

    }

    /**
     * Test of divide method, of class ComplexNumber.
     */
    @Test
    public void testDivide() {
        System.out.println("divide");
        ComplexNumber z = new ComplexNumber(2, 2);
        ComplexNumber instance = new ComplexNumber(1, 2);
        ComplexNumber expResult = new ComplexNumber(0.75, 0.25);
        ComplexNumber result = instance.divide(z);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of divide method, of class ComplexNumber.
     */
    @Test
    public void testDivideByZero() {
        System.out.println("divide");
        ComplexNumber z = new ComplexNumber(0, 0);
        ComplexNumber instance = new ComplexNumber(1, 2);
        ComplexNumber expResult = new ComplexNumber(Double.NaN, Double.NaN);
        ComplexNumber result = instance.divide(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ComplexNumber.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new ComplexNumber(3, 1);
        ComplexNumber instance = new ComplexNumber(3, 1);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class ComplexNumber.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("not equals");
        Object obj = new ComplexNumber(3, 1);
        ComplexNumber instance = new ComplexNumber(3, 0);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ComplexNumber.
     */
    @Test
    public void testEqualsNotSameClass() {
        System.out.println("equals different class");
        Scanner s = new Scanner(System.in);
        ComplexNumber instance = new ComplexNumber(3, 0);
        boolean expResult = false;
        boolean result = instance.equals(s);
        assertEquals(expResult, result);
    }
    

    /**
     * Test of toString method, of class ComplexNumber.
     */
    @Test
    public void testToString() {
        System.out.println("toString - positive imaginary");
        ComplexNumber instance = new ComplexNumber(1, 2.1);
        String expResult = "(1.0 + 2.1j)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of toString method, of class ComplexNumber.
     */
    @Test
    public void testToStringNegImg() {
        System.out.println("toString - negative imaginary");
        ComplexNumber instance = new ComplexNumber(1, -2.1);
        String expResult = "(1.0 - 2.1j)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class ComplexNumber.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        ComplexNumber instance = null;
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of showData method, of class ComplexNumber.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        ComplexNumber instance = new ComplexNumber(1, 2.1);
        String expResult = "1.0 + 2.1j";
        String result = instance.showData();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of showData method, of class ComplexNumber.
     */
    @Test
    public void testShowDataNegImg() {
        System.out.println("showData");
        ComplexNumber instance = new ComplexNumber(1, -2.1);
        String expResult = "1.0 - 2.1j";
        String result = instance.showData();
        assertEquals(expResult, result);
    }
}
