/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatorrpn1de_h.controllers;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class CalculatorControllerTest {
    
    public CalculatorControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class CalculatorController.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("1 + 2j");
        instance.addToStack("1.1 + 1.0j");
        int expResult = 0;
        int result = instance.add();
        assertEquals(expResult, result);
        assertEquals(instance.showData(), "[2.1 + 3.0j]");
    }
    
    /**
     * Test of add method, of class CalculatorController.
     */
    @Test
    public void testAddNEO() {
        System.out.println("add - NEO");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("1 + 2j");
        int expResult = -1;
        int result = instance.add();
        assertEquals(expResult, result);
        assertEquals(instance.showData(), "[1.0 + 2.0j]");
    }

    /**
     * Test of subtract method, of class CalculatorController.
     */
    @Test
    public void testSubtract() {
        System.out.println("subtract");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("1 + 2j");
        instance.addToStack("1.0 + 1.0j");
        int expResult = 0;
        int result = instance.subtract();
        assertEquals(expResult, result);
        assertEquals(instance.showData(), "[0.0 - 1.0j]");
    }
    
    /**
     * Test of add method, of class CalculatorController.
     */
    @Test
    public void testSubtractNEO() {
        System.out.println("subtract - NEO");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("1 + 2j");
        int expResult = -1;
        int result = instance.subtract();
        assertEquals(expResult, result);
        assertEquals(instance.showData(), "[1.0 + 2.0j]");
    }

    /**
     * Test of multiply method, of class CalculatorController.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("1 + 2j");
        instance.addToStack("1 - 2j");
        int expResult = 0;
        int result = instance.multiply();
        assertEquals(expResult, result);
        assertEquals(instance.showData(), "[5.0 + 0.0j]");
    }
    
    /**
     * Test of multiply method, of class CalculatorController.
     */
    @Test
    public void testMultiplyNEO() {
        System.out.println("multiply - NEO");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("1 + 2j");
        int expResult = -1;
        int result = instance.multiply();
        assertEquals(expResult, result);
        assertEquals(instance.showData(), "[1.0 + 2.0j]");
    }

    /**
     * Test of divide method, of class CalculatorController.
     */
    @Test
    public void testDivide() {
        System.out.println("divide");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("2.0 + 0.0j");
        instance.addToStack("1 + 2j");
        int expResult = 0;
        int result = instance.divide();
        assertEquals(expResult, result);
        assertEquals(instance.showData(), "[0.5 + 1.0j]");
    }
    
    /**
     * Test of divide method, of class CalculatorController.
     */
    @Test
    public void testDivideNEO() {
        System.out.println("divide");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("1 + 2j");
        int expResult = -1;
        int result = instance.divide();
        assertEquals(expResult, result);
        assertEquals(instance.showData(), "[1.0 + 2.0j]");
    }
    
    /**
     * Test of divide method, of class CalculatorController.
     */
    @Test
    public void testDivideDVZ() {
        System.out.println("divide - DVZ");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("0 + 0j");
        instance.addToStack("1 + 2j");
        int expResult = -2;
        int result = instance.divide();
        assertEquals(expResult, result);
        assertEquals(instance.showData(), "[0.0 + 0.0j, 1.0 + 2.0j]");
    }

    /**
     * Test of addToStack method, of class CalculatorController.
     */
    @Test
    public void testAddToStack() {
        System.out.println("addToStack");
        CalculatorController instance = new CalculatorController();
        String number = "1 + 2j";
        int expResult = 0;  // OK
        int result = instance.addToStack(number);
        assertEquals(expResult, result);
        
        number = "1 - 2.0j";
        result = instance.addToStack(number);
        assertEquals(expResult, result);
        
        assertEquals(instance.showData(), "[1.0 - 2.0j, 1.0 + 2.0j]");
    }
    
    /**
     * Test of addToStack method, of class CalculatorController.
     */
    @Test
    public void testAddToStackWNF() {
        System.out.println("addToStack - wrong number format");
        CalculatorController instance = new CalculatorController();
        String number = "1 + 2j";
        int expResult = 0;  // OK
        int result = instance.addToStack(number);
        assertEquals(expResult, result);
        
        number = "1 - abcj";
        expResult = -1;
        result = instance.addToStack(number);
        assertEquals(expResult, result);
        
        assertEquals(instance.showData(), "[1.0 + 2.0j]");
    }

    /**
     * Test of drop method, of class CalculatorController.
     */
    @Test
    public void testDrop() {
        System.out.println("drop");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("0 + 0j");
        instance.addToStack("1 - 2j");
        instance.addToStack("1 + 2j");
        int expResult = 0;
        int result = instance.drop();
        assertEquals(expResult, result);
        assertEquals(instance.showData(), "[1.0 - 2.0j, 0.0 + 0.0j]");
    }
    
    /**
     * Test of drop method, of class CalculatorController.
     */
    @Test
    public void testDropNOS() {
        System.out.println("drop - Nothing os stack");
        CalculatorController instance = new CalculatorController();
        int expResult = -1;
        int result = instance.drop();
        assertEquals(expResult, result);
        assertEquals(instance.showData(), "[]");
    }

    /**
     * Test of toString method, of class CalculatorController.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("0 + 0j");
        instance.addToStack("1 + 2j");
        String expResult = "[1.0 + 2.0j, 0.0 + 0.0j]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of showData method, of class CalculatorController.
     */
    @Test
    public void testShowData() {
        System.out.println("showData");
        CalculatorController instance = new CalculatorController();
        instance.addToStack("0 + 0j");
        instance.addToStack("1 + 2j");
        String expResult = "[1.0 + 2.0j, 0.0 + 0.0j]";
        String result = instance.showData();
        assertEquals(expResult, result);
    }
    
}
