/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatorrpn1de_h;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class ComplexNumberStackTest {
    
    public ComplexNumberStackTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of push method, of class ComplexNumberStack.
     */
    @Test
    public void testPush() {
        System.out.println("push - non empty stack");
        ComplexNumber z = new ComplexNumber(3, -1);
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1, 2));
        instance.push(new ComplexNumber(2, 0));
        instance.push(z);
        ComplexNumber res = instance.pop();
        assertEquals(res, z);
    }
    
    /**
     * Test of push method, of class ComplexNumberStack.
     */
    @Test
    public void testPushEmpty() {
        System.out.println("push - empty stack");
        ComplexNumber z = new ComplexNumber(1, 2);
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(z);
        ComplexNumber res = instance.pop();
        assertEquals(res, z);
    }

    /**
     * Test of pop method, of class ComplexNumberStack.
     */
    @Test
    public void testPop() {
        System.out.println("pop - empty stack");
        ComplexNumberStack instance = new ComplexNumberStack();
        ComplexNumber result = instance.pop();
        assertNull(result);
    }

    /**
     * Test of toString method, of class ComplexNumberStack.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1, 2.3));
        instance.push(new ComplexNumber(2, 2.3));
        instance.push(new ComplexNumber(3, 2.3));
        String expResult = "[3.0 + 2.3j, 2.0 + 2.3j, 1.0 + 2.3j]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of toString method, of class ComplexNumberStack.
     */
    @Test
    public void testToStringEmptyStack() {
        System.out.println("toString");
        ComplexNumberStack instance = new ComplexNumberStack();
        String expResult = "[]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ComplexNumberStack.
     */
    @Test
    public void testEqualsEmptyStack() {
        System.out.println("equals - empty stack");
        Object obj = new ComplexNumberStack();
        ComplexNumberStack instance = new ComplexNumberStack();
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class ComplexNumberStack.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new ComplexNumberStack();
        ((ComplexNumberStack)obj).push(new ComplexNumber(1, 2.3));
        ((ComplexNumberStack)obj).push(new ComplexNumber(2, 2.3));
        ((ComplexNumberStack)obj).push(new ComplexNumber(3, 2.3));
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1, 2.3));
        instance.push(new ComplexNumber(2, 2.3));
        instance.push(new ComplexNumber(3, 2.3));
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class ComplexNumberStack.
     */
    @Test
    public void testEqualsNotSS() {
        System.out.println("equals not - same size");
        Object obj = new ComplexNumberStack();
        ((ComplexNumberStack)obj).push(new ComplexNumber(1, 2.3));
        ((ComplexNumberStack)obj).push(new ComplexNumber(2, -2.3));
        ((ComplexNumberStack)obj).push(new ComplexNumber(3, 2.3));
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1, 2.3));
        instance.push(new ComplexNumber(2, 2.3));
        instance.push(new ComplexNumber(3, 2.3));
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class ComplexNumberStack.
     */
    @Test
    public void testEqualsNotDS() {
        System.out.println("equals not - different size");
        Object obj = new ComplexNumberStack();
        ((ComplexNumberStack)obj).push(new ComplexNumber(1, 2.3));
        ((ComplexNumberStack)obj).push(new ComplexNumber(2, 2.3));
        ComplexNumberStack instance = new ComplexNumberStack();
        instance.push(new ComplexNumber(1, 2.3));
        instance.push(new ComplexNumber(2, 2.3));
        instance.push(new ComplexNumber(3, 2.3));
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
}
