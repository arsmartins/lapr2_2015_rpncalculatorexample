/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatorrpn1de_h;

import TOCS.iTOCS;

/**
 *
 * @author amartins
 */
public class ComplexNumber implements iTOCS{
    private double a, b;
    private final double ME = 0.000005;
    
    public ComplexNumber(double real, double imaginary){
        this.a = real;
        this.b = imaginary;
    }
    
    public ComplexNumber add(ComplexNumber z){
        return new ComplexNumber(this.a + z.a, this.b + z.b);
    }
    
    public ComplexNumber subtract(ComplexNumber z){
        return new ComplexNumber(this.a - z.a, this.b - z.b);
    }
    
    public ComplexNumber multiply(ComplexNumber z){
        return new ComplexNumber(this.a * z.a - this.b * z.b,
            this.b * z.a + this.a * z.b);
    }
    
    public ComplexNumber divide(ComplexNumber z){
        ComplexNumber result = new ComplexNumber(
            (this.a * z.a + this.b * z.b)/(z.a*z.a + z.b*z.b),
            (this.b * z.a - this.a * z.b)/(z.a*z.a + z.b*z.b));
        return result;
    }
    
    @Override
    public boolean equals(Object obj){
        boolean res = false;
        if(obj instanceof ComplexNumber) {
            ComplexNumber aux = (ComplexNumber) obj;
            if(Double.isNaN(aux.a) && Double.isNaN(this.a) && 
                    Double.isNaN(aux.b) && Double.isNaN(this.b))
                res = true;
            else
                res = Math.abs(aux.a - this.a)<this.ME && Math.abs(aux.b - this.b)<this.ME;
        }
        return res;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.a) ^ (Double.doubleToLongBits(this.a) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.b) ^ (Double.doubleToLongBits(this.b) >>> 32));
        return hash;
    }
    
    @Override
    public String toString(){
        if(this.b >= 0)
            return "(" + this.a + " + " + this.b + "j)";
        else
            return "(" + this.a + " - " + (-this.b) + "j)";
    }
    
    @Override
    public String showData(){
        if(this.b >= 0)
            return this.a + " + " + this.b + "j";
        else
            return this.a + " - " + (-this.b) + "j";
    }

}
