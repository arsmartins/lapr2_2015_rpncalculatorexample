/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatorrpn1de_h;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import TOCS.iTOCS;

/**
 *
 * @author amartins
 */
public class ComplexNumberStack implements iTOCS {
    private ArrayDeque<ComplexNumber> stack;
    
    public ComplexNumberStack(){
        stack = new ArrayDeque<ComplexNumber>();
    }
    
    public void push(ComplexNumber z){
        stack.push(z);
    }
    
    public ComplexNumber pop(){     // a simple if(this.stack.size()>0) would also work
        try {
            return stack.pop();
        } catch (NoSuchElementException e) {
            return null;
        }
    }
    
    public int size(){
        return stack.size();
    }
    
    @Override
    public String toString(){
        String aux = "[";
        if(this.stack.size()>0){
            Iterator<ComplexNumber> it1 = this.stack.iterator();
            aux = aux + it1.next().toString();
            while(it1.hasNext())
                aux = aux + ", " + it1.next().toString();
        }
        return aux+"]";
    }
    
    @Override
    public String showData(){
        return this.toString();
    }
    
    @Override
    public boolean equals(Object obj){
        boolean res = false;
        
        if(this == obj)
            return true;
        
        if(obj instanceof ComplexNumberStack){
             ArrayDeque<ComplexNumber> aux = (ArrayDeque<ComplexNumber>) 
                     ((ComplexNumberStack) obj).stack;
                if(aux.size() == this.stack.size()) {
                    res = true;
                    Iterator<ComplexNumber> it2 = this.stack.iterator();
                    Iterator<ComplexNumber> it1 = aux.iterator();
                    while(it1.hasNext() && it2.hasNext() && res) {
                        ComplexNumber z1 = it1.next();
                        ComplexNumber z2 = it2.next();
                        res = z1.equals(z2);
                    }
                } 
        }
        return res;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.stack);
        return hash;
    }
}
