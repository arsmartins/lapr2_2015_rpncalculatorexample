/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatorrpn1de_h.controllers;

import calculatorrpn1de_h.ComplexNumber;
import calculatorrpn1de_h.ComplexNumberStack;
import TOCS.iTOCS;

/**
 *
 * @author amartins
 */
public class CalculatorController implements iTOCS {
    private final ComplexNumberStack stack;
    
    public CalculatorController(){
        this.stack = new ComplexNumberStack();
    }
    
    public int add(){
        ComplexNumber z1 = this.stack.pop();
        if(z1 != null) {
            ComplexNumber z2 = this.stack.pop();
            if(z2 != null) {
                this.stack.push(z1.add(z2));
                return 0;
            }
            else
               this.stack.push(z1);                   
        }
        return -1;
    }
    
    public int subtract(){
        ComplexNumber z1 = this.stack.pop();
        if(z1 != null) {
            ComplexNumber z2 = this.stack.pop();
            if(z2 != null) {
                this.stack.push(z1.subtract(z2));
                return 0;
            }
            else
               this.stack.push(z1);                   
        }
        return -1;
    }
    
    public int multiply(){
        ComplexNumber z1 = this.stack.pop();
        if(z1 != null) {
            ComplexNumber z2 = this.stack.pop();
            if(z2 != null) {
                this.stack.push(z1.multiply(z2));
                return 0;
            }
            else
               this.stack.push(z1);                   
        }
        return -1;
    }
    
    public int divide(){
        ComplexNumber z1 = this.stack.pop();
        if(z1 != null) {
            ComplexNumber z2 = this.stack.pop();
            if(z2 != null) {
                ComplexNumber zRes = z1.divide(z2);
                if(!zRes.equals(new ComplexNumber(Double.NaN, Double.NaN))){
                    this.stack.push(z1.divide(z2));
                    return 0;
                }
                else {      // division by zero
                    this.stack.push(z1);
                    this.stack.push(z2);
                    return -2;
                }                    
            }
            else
               this.stack.push(z1);                   
        }
        return -1;
    }
    
    public int addToStack(String number){
        ComplexNumber z = parseNumber(number);
        if(z!=null){
            this.stack.push(z);
            return 0;
        }
        else
            return -1;
    }
    
    public int drop(){
        if(this.stack.size()>0){
            this.stack.pop();
            return 0;
        }
            return -1;
    }
    
    @Override
    public String toString(){
        return this.stack.toString();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String showData(){
        return this.stack.showData();
    }
    
    /**
     * Parses a complex number in either polar or rectangular format
     * 
     * @param st
     * @return 
     */
    private ComplexNumber parseNumber(String st){
        String aux = trimSpaces(st);
        if (aux.length() == 0) {
            return null;
        }
       
        int p = aux.indexOf("exp(");
        if (p > 0) {    // polar form
            try {
                double base = Double.valueOf(aux.substring(0, p - 1));
                double exponent = Double.valueOf(aux.substring(p + 4, aux.length() - 2)); // assumes the number ends in "j)" or "i)"
                return new ComplexNumber(base * Math.cos(exponent), base * Math.sin(exponent));
            } catch (NumberFormatException e) {
                return null;
            }
        } else {     // rectangular form
            aux = aux.replace("-", "#-");
            aux = aux.replace("+", "#+");
            String aux1[] = aux.split("#");
            
            if (aux1.length == 1) {   // real or imaginary only
                double img = 0d;
                double re = 0d;
                p = aux1[0].indexOf("j");
                try {
                    if (p >= 0) {
                        img = Double.valueOf(aux1[0].substring(0, p));
                    } else {
                        p = aux1[0].indexOf("i");
                        if (p >= 0) {
                            img = Double.valueOf(aux1[0].substring(0, p));
                        } else {
                            re = Double.valueOf(aux1[0]);
                        }
                    }
                    return new ComplexNumber(re, img);
                } catch (NumberFormatException e) {
                    return null;
                }
            } else if (aux1.length == 2) {     // real and imaginary components
                double img = 0d;
                double re = 0d;
                try {
                    re = Double.valueOf(aux1[0]);

                    p = aux1[1].indexOf("j");
                    if (p >= 0) {
                        img = Double.valueOf(aux1[1].substring(0, p));
                    } else {
                        p = aux1[1].indexOf("i");
                        if (p >= 0) {
                            img = Double.valueOf(aux1[1].substring(0, p));
                        } else {
                            return null;    // there is no imaginary component
                        }
                    }
                } catch (NumberFormatException e) {
                    return null;
                }
                return new ComplexNumber(re, img);
            } else // length > 2
            {
                return null;
            }
        }
    }
    
    /**
     * Eliminate spaces within the string
     *
     * @param st
     * @return
     */
    private String trimSpaces(String st) {
        String vecSt[] = st.split(" ");

        if (vecSt.length > 1) {
            String resSt = "";
            for (String aux : vecSt) 
                    resSt += aux;
            return resSt;
        } else {
            return st;
        }
    }
}
